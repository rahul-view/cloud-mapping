import pandas
import json
import sys
import os
import requests
import time
import argparse

def login(email_id, pwd):
    url = "https://api-demo.viewcorp.xyz/api/v1/auth/login"

    payload = {"email": email_id, "password": pwd, "role": "customer"}
    payload = json.dumps(payload)
    headers = {
      'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data = payload)

    access_token = json.loads(response.text)['data']['access_token']
    return access_token

def get_ids(access_token):
    url = "https://api-demo.viewcorp.xyz/api/v1/buildings"

    payload = {}
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {}'.format(access_token)
    }
    response = requests.request("GET", url, headers=headers, data = payload)
    resp = json.loads(response.text)['data']


    for ids in resp:

        company_id = ids['companyId']
        building_id = ids['uniqueId']

    return company_id, building_id

def register_igu(companyId, buildingId, physical_id, logical_id, access_token):
    url = "https://api-demo.viewcorp.xyz/api/v1/pairing/register"

    payload = {"companyId": companyId,
               "buildingId": buildingId,
               "deviceData": [{"serial": physical_id,
                               "logicalId": logical_id,
                               "deviceType": "IGU"}]}
    payload = json.dumps(payload)
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {}'.format(access_token)
    }

    response = requests.request("POST", url, headers=headers, data = payload)
    resp = json.loads(response.text)
    return resp

def unregister_igu(companyId, buildingId, physical_id, logical_id, access_token):
    url = "https://api-demo.viewcorp.xyz/api/v1/pairing/unregister"

    payload = {"companyId": companyId,
               "buildingId": buildingId,
               "deviceData": [{"serial": physical_id,
                               "logicalId": logical_id,
                               "deviceType": "IGU"}]}
    payload = json.dumps(payload)
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {}'.format(access_token)
    }

    response = requests.request("POST", url, headers=headers, data = payload)
    resp = json.loads(response.text)
    return resp



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="IGU Mapping")
    # defining arguments for parser object
    parser.add_argument("-e", "--email_id", type=str, required=True,
                        metavar="email_id", default=None,
                        help="Pass customer email_id")
    parser.add_argument("-p", "--password", type=str, required=True,
                        metavar="password", default=None,
                        help="Enter customer password")
    parser.add_argument("-f", "--path", type=str, required=True,
                        metavar="path", default=None,
                        help="Enter the absolute file path")

    args = parser.parse_args()
    email_id = args.email_id
    password = args.password
    file_path = args.path

    # Get access token
    access_token = login(email_id, password)

    # Returns company and building IDs
    company_id, building_id = get_ids(access_token)

    #Read the csv file having lite ID's and logical ID's. Assuming 1st column is lite ID and 2nd column is logical ID
    df = pandas.read_csv(file_path)

    # Get lite ID's and logical ID's
    igu_mac = list(df.iloc[:,0])
    igu_lid = list(df.iloc[:,1])

    # #IGU mapping

    for i in range(len(igu_mac)):
        # print(igu_mac[i], igu_lid[i])
        try:
            print(register_igu(company_id, building_id, igu_mac[i], igu_lid[i],access_token))
        except:
            pass
